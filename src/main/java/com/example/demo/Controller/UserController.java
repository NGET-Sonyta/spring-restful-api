package com.example.demo.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class UserController {

    @GetMapping("/user")
    public String getUser(){
        int a=10/0;
        return "Some Users";
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handler(){
        return new ResponseEntity<>("Exception Method Works.", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
