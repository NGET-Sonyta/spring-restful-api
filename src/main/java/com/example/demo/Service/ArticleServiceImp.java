package com.example.demo.Service;

import com.example.demo.Model.Article;
import com.example.demo.Repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService{
    @Autowired
    ArticleRepository articleRepository;

    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id-1);
    }

    @Override
    public void insert(Article article) {
    articleRepository.insert(article);
    }

    @Override
    public void update(int id, Article article) {
    articleRepository.update(id-1,article);
    }
}
