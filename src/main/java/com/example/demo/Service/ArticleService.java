package com.example.demo.Service;

import com.example.demo.Model.Article;

import java.util.List;

public interface ArticleService {

    List<Article> findAll();
    void delete(int id);
    void insert(Article article);
    void update(int id, Article article);

}
