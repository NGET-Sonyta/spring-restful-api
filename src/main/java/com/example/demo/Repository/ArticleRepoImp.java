package com.example.demo.Repository;

import com.example.demo.Model.Article;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;
@Repository
public class ArticleRepoImp implements ArticleRepository {

    static List<Article> articles;
    {
        articles=new ArrayList<>();
        articles.add(new Article(1,"Harry Potter", "It's amazing","Fiction"));
        articles.add(new Article(2,"Beauty and the Beast", "Very Recommended","Fairytale"));

    }

    @Override
    public List<Article> findAll() {
        return articles;
    }

    @Override
    public void delete(int id) {
    articles.remove(id-1);
    }

    @Override
    public void update(int id, Article article) {
    Article article1=articles.get(id);
    article1.setId(article.getId());
    article1.setName(article.getName());
    article1.setDescription(article.getDescription());
    article1.setCategory(article.getCategory());
    articles.add(article1);
    }

    @Override
    public void insert(Article article) {
    articles.add(article);
    }


}
