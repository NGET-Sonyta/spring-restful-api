package com.example.demo.Repository;

import com.example.demo.Model.Article;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ArticleRepository {
    public List<Article> findAll();
    void delete(int id);
    void update(int id, Article article);
    void insert(Article article);
}
